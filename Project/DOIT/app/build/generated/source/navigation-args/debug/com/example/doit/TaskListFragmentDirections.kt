package com.example.doit

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections

public class TaskListFragmentDirections private constructor() {
  public companion object {
    public fun actionTaskListFragmentToTasksFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_taskListFragment_to_tasksFragment)
  }
}
