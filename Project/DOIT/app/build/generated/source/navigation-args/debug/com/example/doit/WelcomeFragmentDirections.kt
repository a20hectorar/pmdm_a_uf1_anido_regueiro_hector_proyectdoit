package com.example.doit

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections

public class WelcomeFragmentDirections private constructor() {
  public companion object {
    public fun actionWelcomeFragmentToTasksFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_welcomeFragment_to_tasksFragment)
  }
}
