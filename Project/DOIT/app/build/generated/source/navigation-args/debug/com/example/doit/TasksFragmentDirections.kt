package com.example.doit

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections

public class TasksFragmentDirections private constructor() {
  public companion object {
    public fun actionTasksFragmentToTaskListFragment(): NavDirections =
        ActionOnlyNavDirections(R.id.action_tasksFragment_to_taskListFragment)
  }
}
