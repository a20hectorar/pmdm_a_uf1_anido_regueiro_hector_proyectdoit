// Generated by view binder compiler. Do not edit!
package com.example.doit.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.example.doit.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class FragmentWelcomeBinding implements ViewBinding {
  @NonNull
  private final FrameLayout rootView;

  @NonNull
  public final Button buttonStart;

  @NonNull
  public final FrameLayout welcomeFragment;

  private FragmentWelcomeBinding(@NonNull FrameLayout rootView, @NonNull Button buttonStart,
      @NonNull FrameLayout welcomeFragment) {
    this.rootView = rootView;
    this.buttonStart = buttonStart;
    this.welcomeFragment = welcomeFragment;
  }

  @Override
  @NonNull
  public FrameLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static FragmentWelcomeBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static FragmentWelcomeBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.fragment_welcome, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static FragmentWelcomeBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.button_start;
      Button buttonStart = ViewBindings.findChildViewById(rootView, id);
      if (buttonStart == null) {
        break missingId;
      }

      FrameLayout welcomeFragment = (FrameLayout) rootView;

      return new FragmentWelcomeBinding((FrameLayout) rootView, buttonStart, welcomeFragment);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
