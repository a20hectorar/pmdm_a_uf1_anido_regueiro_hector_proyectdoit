// Generated by view binder compiler. Do not edit!
package com.example.doit.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.example.doit.R;
import com.google.android.material.chip.Chip;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class FragmentTasksBinding implements ViewBinding {
  @NonNull
  private final ScrollView rootView;

  @NonNull
  public final ConstraintLayout addTaskLayout;

  @NonNull
  public final Button buttonAdd;

  @NonNull
  public final Chip chip;

  @NonNull
  public final Chip chipResaltar;

  @NonNull
  public final EditText editText20;

  @NonNull
  public final EditText editText21;

  @NonNull
  public final RadioGroup radioGroup;

  @NonNull
  public final RadioButton radioNonPriority;

  @NonNull
  public final RadioButton radioPriority;

  @NonNull
  public final ScrollView tasksFragment;

  @NonNull
  public final TextView textView;

  @NonNull
  public final TextView textView2;

  private FragmentTasksBinding(@NonNull ScrollView rootView,
      @NonNull ConstraintLayout addTaskLayout, @NonNull Button buttonAdd, @NonNull Chip chip,
      @NonNull Chip chipResaltar, @NonNull EditText editText20, @NonNull EditText editText21,
      @NonNull RadioGroup radioGroup, @NonNull RadioButton radioNonPriority,
      @NonNull RadioButton radioPriority, @NonNull ScrollView tasksFragment,
      @NonNull TextView textView, @NonNull TextView textView2) {
    this.rootView = rootView;
    this.addTaskLayout = addTaskLayout;
    this.buttonAdd = buttonAdd;
    this.chip = chip;
    this.chipResaltar = chipResaltar;
    this.editText20 = editText20;
    this.editText21 = editText21;
    this.radioGroup = radioGroup;
    this.radioNonPriority = radioNonPriority;
    this.radioPriority = radioPriority;
    this.tasksFragment = tasksFragment;
    this.textView = textView;
    this.textView2 = textView2;
  }

  @Override
  @NonNull
  public ScrollView getRoot() {
    return rootView;
  }

  @NonNull
  public static FragmentTasksBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static FragmentTasksBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.fragment_tasks, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static FragmentTasksBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.add_task_layout;
      ConstraintLayout addTaskLayout = ViewBindings.findChildViewById(rootView, id);
      if (addTaskLayout == null) {
        break missingId;
      }

      id = R.id.button_add;
      Button buttonAdd = ViewBindings.findChildViewById(rootView, id);
      if (buttonAdd == null) {
        break missingId;
      }

      id = R.id.chip;
      Chip chip = ViewBindings.findChildViewById(rootView, id);
      if (chip == null) {
        break missingId;
      }

      id = R.id.chip_resaltar;
      Chip chipResaltar = ViewBindings.findChildViewById(rootView, id);
      if (chipResaltar == null) {
        break missingId;
      }

      id = R.id.editText20;
      EditText editText20 = ViewBindings.findChildViewById(rootView, id);
      if (editText20 == null) {
        break missingId;
      }

      id = R.id.editText21;
      EditText editText21 = ViewBindings.findChildViewById(rootView, id);
      if (editText21 == null) {
        break missingId;
      }

      id = R.id.radio_group;
      RadioGroup radioGroup = ViewBindings.findChildViewById(rootView, id);
      if (radioGroup == null) {
        break missingId;
      }

      id = R.id.radio_nonPriority;
      RadioButton radioNonPriority = ViewBindings.findChildViewById(rootView, id);
      if (radioNonPriority == null) {
        break missingId;
      }

      id = R.id.radio_priority;
      RadioButton radioPriority = ViewBindings.findChildViewById(rootView, id);
      if (radioPriority == null) {
        break missingId;
      }

      ScrollView tasksFragment = (ScrollView) rootView;

      id = R.id.textView;
      TextView textView = ViewBindings.findChildViewById(rootView, id);
      if (textView == null) {
        break missingId;
      }

      id = R.id.textView2;
      TextView textView2 = ViewBindings.findChildViewById(rootView, id);
      if (textView2 == null) {
        break missingId;
      }

      return new FragmentTasksBinding((ScrollView) rootView, addTaskLayout, buttonAdd, chip,
          chipResaltar, editText20, editText21, radioGroup, radioNonPriority, radioPriority,
          tasksFragment, textView, textView2);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
